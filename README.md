
Please note that this repo is deprecated. You can find new demo app here: https://bitbucket.org/sukdev/streamamg-android-app/src/master/
======




Requisites
======

* Android Studio 2.2.1 or higher


Getting Started
======

Clone SDK repository and clone Kaltura-Android-app repository inside the SDK repository folder.

Open SDK project in Android Studio.

Run kaltura-android-app.


### Settings ###

Kaltura-android-app > MainActivity.java :
```
public static final String SERVICE_URL = "https://{server_mp}";
public static final String PARTNER_ID = "{partner_id}";
public static final String UI_CONF_ID = "{uiconf_id}";
public static final String ENTRY_ID = "{entry_id}";
```

For more examples you can check kaltura-android-sdk\KalturaDemos.